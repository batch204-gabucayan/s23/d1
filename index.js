// alert('YOU ARE IN MASTER!')

/*
	Objects
		- An object is a data type that is used to represent a real world object
		- It is a collection of related data and/or functionalities
		- Information is stored in object represented in 'key: value' pair
			key -> property of the object
			value -> actual data to be stored

		Two ways of creating objects in JavaScript
			1. Object Literal Notation
				- let/cons objectName = {}
			2. Object Constructor Notation
				- Object Instantiation ( let object = new Object() )

	Object Literal Notation

		Syntax:
			let/const = {
				keyA: valueA,
				keyB: valueB
			}

*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using literal notation:");
console.log(cellphone);

console.log(typeof cellphone);

let cellphone2 = {
	name: "Motorola",
	manufactureDate: 2000
};

console.log(cellphone2);

//Creating objects using constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
	 

	
	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters

function Laptop(name, manufactureDate) {
	//this.propertyName = value
	this.nameProperty = name; // this will construct
	this.manufactureDateProperty = manufactureDate;
}

// This is a unique instance of the Laptop object
/*
    - The "new" operator creates an instance of an object
    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/

let laptop = new Laptop('Lenovo', 2008); // pascal case to differentiate to regular function
console.log(laptop);

let oldLaptop = Laptop('IBM', 1980);
console.log(oldLaptop); // undefined

let myLaptop = new Laptop('MacBook', 2020);
console.log(myLaptop);


// Creating empty objects
let computer = {};
console.log(computer);

let myComputer = new Object();
console.log(myComputer);

let yourComputer = new Laptop(); // will have keys but no values
console.log(yourComputer);

// Accessing Object Properties

// Using the dot notation
console.log(myLaptop.nameProperty);
console.log(myLaptop.manufactureDateProperty);

// Using the square bracket notation
console.log(myLaptop['nameProperty'])
//Accessing array objects
//let array = [{name: "Lenovo", manufactureDate: 2008} , {name: "MacBook", manufactureDate: 2020}]
let array = [laptop, myLaptop]; //MacBook
console.log(array);

//arrayName[indexNumber].propertyName
//This tells us the array[1] is an object by using the dot
console.log(array[1].name); //MacBook

console.log(array[1]['name']);


//Initializing/Adding/Deleting/Reassigning Object Properties
let car = {};
console.log(car);

//objectName.propertyName = value
car.name = 'Sarao';
console.log("Result from adding properties using dot notation:");
console.log(car);

car['manufacture date'] = 2019;
console.log(car);

//Deleting object properties
delete car['manufacture date'];
console.log(car);

car.manufactureDate = 2019;
console.log(car);

//Reassigning object properties
car.name = "Mustang";
console.log(car);

//Object Methods
/*
	-A method is a function which is a property of an object
	-They are also functions and one of the key differences they have is that methods are functions related to a specific object
	-Methods are useful for creating object specific function which are used to perform tasks on them

*/

let person = {
	name: 'Jack',
	talk: function() {
		console.log('Hello! My name is ' + this.name);
	}
}
console.log(person);
person.talk();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.");
}

person.walk();

// Method are useful for creating reusable functions that performs tasks related to object

let friend = {
	firstName: 'Rafael',
	lastName: 'Santillan',
	address: {
		city: 'Quezon City',
		country: 'Philippines',
	},
	email:['raf@mail.com', 'raf123@yahoo.com'],

	introduce: function() {
		console.log('Hello! My name is '+this.firstName+" "+this.lastName);
	}
}
friend.introduce();

// Real World Application of Objexts
/*
	Scenario:
		1. We would like to create a game that would have several pokemon that interact with each other
		2 Every pokemon would have the same set of stats, properties, and function
*/

let myPokemon ={
	name:'Pikachu',
	level: 3,
	health: 100,
	attack: 50,

	tackle: function() {
		console.log("This Pokemon tackle target pokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},

	faint: function(){
		console.log("Pokemon fainted.");
	}
}

console.log(myPokemon);

//

function Pokemon(name, level, health) {
	this.pokeName = name;
	this.pokeLevel = level;
	this.pokeHealth = health;
	this.pokeAttack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.pokeName+" tackled "+target.pokeName);
		target.pokeHealth -= this.pokeAttack; // will continously decrease health
		// target.pokeHealth = target.pokeHealth - this.pokeAttack; // haha lol
		if(target.pokeHealth >= 5){
		console.log(target.pokeName+"'s health is now reduced to "+target.pokeHealth);
		} else {
			console.log(target.pokeName+"'s health drops to zero.")
			target.faint();
		}
	},
	this.faint = function() {
		console.log(this.pokeName+" fainted");
	}
}

let squirtle = new Pokemon('Squirtle', 99, 200);
let snorlax = new Pokemon('Snorlax', 75, 500);

console.log(squirtle);
console.log(snorlax);

snorlax.tackle(squirtle);
snorlax.tackle(squirtle);
snorlax.tackle(squirtle);

/*
Mini-Activity:
1. Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuosly as many times the tackle is invoked.
	(target.health - this.attck)
2. If health is less than or equal to 5, invoke faint function
*/